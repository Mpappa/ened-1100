fid = open('Exams.txt','r')

List = fid.readlines()
List = [float(i) for i in List]
        
N = len(List)

a = 0
b = 0
c = 0
d = 0
f = 0

for k in range (N):
    if List[k] >= 90:
        a = a + 1
    elif List[k] >= 80:
        b = b + 1
    elif List[k] >= 70:
        c= c + 1
    elif List[k] >= 60:
        d = d + 1
    else:
        f = f + 1

apercent = (a / N) * 100
bpercent = (b / N) * 100
cpercent = (c / N) * 100
dpercent = (d / N) * 100
fpercent = (f / N) * 100

print('{0:.2f}% A, {1:.2f}% B, {2:.2f}% C, {3:.2f}% D, {4:.2f}% F \n'.format(apercent,bpercent,cpercent,dpercent,fpercent))
