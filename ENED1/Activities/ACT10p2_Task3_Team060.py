# Activity 10.2.1: Task 1
# File: ACT10p2_Task1_TEAMXXX_Template.py
# Date:    30 October 2019
# By:      Michael Pappa
# Section: 005
# Team:    060
# 
# ELECTRONIC SIGNATURE
# Michael Pappa
#
# The electronic signature above indicates the script
# submitted for evaluation is my individual work, and I
# have a general understanding of all aspects of its
# development and execution.
#
# A BRIEF DESCRIPTION OF WHAT THE SCRIPT OR FUNCTION DOES
# This program is a header template that will be used for all the 
# python files the rest of the semester.

V = float(input('Enter the value for fluid velocity (mi/hr)'))
L = float(input('Enter the value for the length(in)'))
u = float(input('Enter the value for the dynamic viscosity of the fluid (lb/in*s)'))
p = float(input("Enter the valure for the density of the fluid (lb/in^3)"))

V = V*0.44704
L = L*0.0254
u = u*17.8579673
p = p*27679.9047

R = (p*V*L)/u

print('The Reynolds number is {0:0.3f} \n'.format(R))