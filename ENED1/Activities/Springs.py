def Springs(K1,K2,F,Config):
    if K1 < 0 or K2 <0 or F < 0:
        print('Error: Invalid Inputs')
    else:
        if Config == 'Series':
            F1 = F
            F2 = F
            Keq = (K1*K2)/(K1+K2)
            x1 = F1/K1
            x2 = F2/K2
            xt = x1 + x2
        elif Config == 'Parallel':
            Keq = K1 + K2
            xt = F/Keq
            x1 = xt
            x2 = xt
            F1 = K1 * x1
            F2 = K2 * x2
    return Keq,F1,F2,x1,x2,xt
