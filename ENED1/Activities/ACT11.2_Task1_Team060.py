# Activity 11.2: Task 1
# File: ACT11.2_Task1_TEAM060.py
# Date:    6 November 2019
# By:      Michael Pappa
# Section: 005
# Team:    060
# 
# ELECTRONIC SIGNATURE
# Michael Pappa
#
# The electronic signature above indicates the script
# submitted for evaluation is my individual work, and I
# have a general understanding of all aspects of its
# development and execution.
#
# A BRIEF DESCRIPTION OF WHAT THE SCRIPT OR FUNCTION DOES
# Check pH value given by user

pH = float(input('Enter the value for pH \n'))

if pH >= 0 and pH < 7 :
    print('The pH is Acidic')
elif pH == 7 :
    print('The pH is Neutral')
elif pH > 7 and pH <= 14 :
    print('The pH is Basic')
else :
    print('Invalid')