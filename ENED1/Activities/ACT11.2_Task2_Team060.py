# Activity 11: Task 2
# File: ACT11p2_Task2_TEAM060.py
# Date:    6 November 2019
# By:      Michael Pappa
# Section: 005
# Team:    060
# 
# ELECTRONIC SIGNATURE
# Michael Pappa
#
# The electronic signature above indicates the script
# submitted for evaluation is my individual work, and I
# have a general understanding of all aspects of its
# development and execution.
#
# A BRIEF DESCRIPTION OF WHAT THE SCRIPT OR FUNCTION DOES
# Find the Total K,X

K1 = float(input('Input the value of the first spring constant\n'))
K2 = float(input('Input the value of the second spring constant\n'))
Ft = float(input('Input the value for the total Force\n'))
configuration = input('Input configuration of the spring, Series or Parallel\n')

if K1 <= 0 or K2 <= 0 or Ft <= 0 :
    print('Invalid')
elif configuration == 'Series' :
    Keq = (K1*K2)/(K1+K2)
    F1 = Ft
    F2 = Ft
    x1 = F1/K1
    x2 = F2/K2
    xt = x1 + x2
    print('Keq equals: {0:.2f} (N/m) \n'.format(Keq))
    print('F1 equals: {0:.2f} (N) \n'.format(F1))
    print('F2 equals: {0:.2f} (N) \n'.format(F2))
    print('x1 equals: {0:.2f} (m) \n'.format(x1))
    print('x2 equals: {0:.2f} (m) \n'.format(x2))
    print('xtotal equals: {0:.2f} (m) \n'.format(xt))
else :
    Keq = K1 + K2
    xt = Ft/Keq
    x1 = xt
    x2 = xt
    F1 = K1*x1
    F2 = K2*x2
    print('Keq equals: {0:.2f} (N/m) \n'.format(Keq))
    print('F1 equals: {0:.2f} (N) \n'.format(F1))
    print('F2 equals: {0:.2f} (N) \n'.format(F2))
    print('x1 equals: {0:.2f} (m) \n'.format(x1))
    print('x2 equals: {0:.2f} (m) \n'.format(x2))
    print('xtotal equals: {0:.2f} (m) \n'.format(xt))





