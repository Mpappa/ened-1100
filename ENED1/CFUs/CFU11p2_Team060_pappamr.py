# CFU 11.2
# File: CFU11p2_Team060_pappamr.py
# Date:    6 November 20190
# By:      Michael Pappa
# Section: 005
# Team:    060
# 
# ELECTRONIC SIGNATURE
# Michael Pappa
#
# The electronic signature above indicates the script
# submitted for evaluation is my individual work, and I
# have a general understanding of all aspects of its
# development and execution.
#
# A BRIEF DESCRIPTION OF WHAT THE SCRIPT OR FUNCTION DOES
# Program outputs description given the selected sound level
import math

Units = input('Input the Units, dB or Pa\n')

if Units == 'dB':
    L = float(input('Input the sound level\n'))
    if L >= 130:
        print('Threshold of pain')
    elif L >= 120 and L < 130:
        print('Hearing Loss')
    elif L >= 100 and L < 120:
        print('Jack Hammer')
    elif L >= 90 and L < 100:
        print('Busy Road Traffic') 
    elif L >= 60 and L < 90:
        print('Normal Conversation')
    elif L >= 30 and L < 60:
        print('Calm Library')               
else:
    P = float(input('Input the sound pressure\n'))
    L = (20*(math.log10((P/(20*(10^-6))))))
    if L >= 130:
        print('Threshold of pain')
    elif L >= 120 and L < 130:
        print('Hearing Loss')
    elif L >= 100 and L < 120:
        print('Jack Hammer')
    elif L >= 90 and L < 100:
        print('Busy Road Traffic') 
    elif L >= 60 and L < 90:
        print('Normal Conversation')
    elif L >= 30 and L < 60:
        print('Calm Library') 