# CFU 10.2
# File: CFU10p2_Team060_pappamr.py
# Date:    30 October 2019
# By:      Michael Pappa
# Section: 005
# Team:    060
# 
# ELECTRONIC SIGNATURE
# Michael Pappa
#
# The electronic signature above indicates the script
# submitted for evaluation is my individual work, and I
# have a general understanding of all aspects of its
# development and execution.
#
# A BRIEF DESCRIPTION OF WHAT THE SCRIPT OR FUNCTION DOES
# This program is a header template that will be used for all the 
# python files the rest of the semester.

e = float(input('Enter the value for emissivity of the object (from 0 to 1)'))
A = float(input('Enter the value for the surface area of the object (m^2)'))
T = float(input('Enter the value for the temperature of the object (K)'))
Tc = float(input('Enter the value of the temperature of the surroundings (K)'))

P = e*5.67e-8*A*((T**4)-(Tc**4))

print('The Net radiation loss is {0:0.2f} W \n'.format(P))