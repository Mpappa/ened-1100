# Homework 11.1
# File: HW11p1_TASK2_TEAM060_pappamr.py
# Date:    14 November 2019
# By:      Michael Pappa
# Section: 005
# Team:    060
# 
# ELECTRONIC SIGNATURE
# Michael Pappa
#
# The electronic signature above indicates the script
# submitted for evaluation is my individual work, and I
# have a general understanding of all aspects of its
# development and execution.
#
# A BRIEF DESCRIPTION OF WHAT THE SCRIPT OR FUNCTION DOES
# Program outputs category based on input of systolic and diastolic pressure

S = float(input('Input Systolic Pressure \n'))
D = float(input('Input Diastolic Pressure \n'))

if S < 50 or S > 239 or D < 35 or D > 135:
    print('Invalid Values')
elif S >= 50 and S <= 89 and D>=35 and D<=59:
    print('Hypotension')
elif S >= 90 and S <= 119 and D>=60 and D<=79:
    print('Normal')
elif S >= 120 and S <= 139 and D>=80 and D<=89:
    print('Pre-hypertension')
elif S >= 140 and S <= 159 and D>=90 and D<=99:
    print('Mild Hypertension')
elif S >= 160 and S <= 179 and D>=100 and D<=109:
    print('Moderate Hypertension')
elif S >= 180 and S <= 209 and D>=110 and D<=119:
    print('Severe Hypertension')
elif S >= 210 and S <= 239 and D>=120 and D<=135:
    print('Very Severe Hypertension')
elif S >= 50 and S <= 89 and D>=60 and D<=79:
    print('Hypotension')
elif S >= 90 and S <= 119 and D>=35 and D<=59:
    print('Hypotension')
elif S >= 90 and S <= 119 and D>=80 and D<=89:
    print('Pre-Hypertension')
elif S >= 90 and S <= 139 and D>=90 and D<=99:
    print('Mild Hypertension')
elif S >= 90 and S <= 159 and D>=100 and D<=109:
    print('Moderate Hypertension')
elif S >= 90 and S <= 179 and D>=110 and D<=119:
    print('Severe Hypertension')
elif S >= 90 and S <= 209 and D>=120 and D<=135:
    print('Very Severe Hypotension')
elif S >= 120 and S <= 139 and D>=60 and D<=79:
    print('Pre-hypertension')
elif S >= 140 and S <= 159 and D>=60 and D<=89:
    print('Mild Hypertension')
elif S >= 160 and S <= 179 and D>=60 and D<=99:
    print('Moderate Hypertension')
elif S >= 180 and S <= 209 and D>=60 and D<=109:
    print('Severe Hypertension')
elif S >= 210 and S <= 239 and D>=60 and D<=119:
    print('Very Sever Hypertension')
