# Homework 10.2
# File: HW10p2_Team060_pappamr.py
# Date:    7 November 2019
# By:      Michael Pappa
# Section: 005
# Team:    060
# 
# ELECTRONIC SIGNATURE
# Michael Pappa
#
# The electronic signature above indicates the script
# submitted for evaluation is my individual work, and I
# have a general understanding of all aspects of its
# development and execution.
#
# A BRIEF DESCRIPTION OF WHAT THE SCRIPT OR FUNCTION DOES
# Program outputs displacement, velocity, acceleration, kinetic energy, and potential energy of an object at time t

import math

A = float(input('Input the value for initial displacement\n'))
K = float(input('Input the value for the spring constant (N/m)\n'))
M = float(input('Input the value for the mass of the suspended object (kg)\n'))
t = float(input('Input the value for the time (s)\n'))

p = A * math.cos(math.sqrt(K / M) * t)
v = -A * math.sqrt(K / M) * math.sin(math.sqrt(K / M) * t)
a = -(A * K) / (M) * math.cos(math.sqrt(K / M) * t)
KE = (1/2) * M * (v)**2
PE = (1/2) * K * (p)**2 

print('Displacement is {0:0.2f}\n'.format(p))
print('Velocity is {0:0.2f} (m/s)\n'.format(v))
print('Acceleration is {0:0.2f} (m/s^2)\n'.format(a))
print('Kinetic Energy is {0:0.2f} (J)\n'.format(KE))
print('Potential Energy is {0:0.2f} (J)\n'.format(PE))