# Homework 13.2: Task 1
# File: 13p2_Task1_TEAM060_pappamr.py
# Date:    28 November 2019
# By:      Michael Pappa
# Section: 005
# Team:    060
# 
# ELECTRONIC SIGNATURE
# Michael Pappa
#
# The electronic signature above indicates the script
# submitted for evaluation is my individual work, and I
# have a general understanding of all aspects of its
# development and execution.
#
# A BRIEF DESCRIPTION OF WHAT THE SCRIPT OR FUNCTION DOES
# This program will include a function that tests validity of inputs and then outputs coordinates of x and y values and their times

import math

Vo = float(input("Enter a value for Vo \n"))

while Vo < 0:
        Vo = float(input("Enter a value for Vo \n"))

Theta = float(input("Enter a value for Theta \n"))

while Theta < 0 or Theta > 90:
        Theta = float(input("Enter a value for Theta \n"))

yo = float(input("Enter a value for yo \n"))

while yo < 0:
        yo = float(input("Enter a value for yo \n"))


def Projectile(Vo,Theta,yo):
    if Vo < 0 or yo < 0 or Theta < 0 or Theta > 90:
        print('Error: Invalid Inputs')
    else:
       for i in range(1, 11):
            T = (Vo * math.sin((math.pi * Theta)/180) + math.sqrt((Vo * math.sin((math.pi * Theta)/180))**2 + 2 * 9.81 * yo))/9.81
            t = (i*T)/10
            if t < T:
                x = Vo * math.cos((math.pi * Theta)/180) * t
                y = -.5 * 9.81 * t**2 + Vo * math.sin((math.pi * Theta)/180) * t + yo
            else:
                x = Vo * math.cos((math.pi * Theta)/180) * t
                y = 0
            print('Height = {0:0.2f}, Distance = {1:0.2f}, Time = {2:0.2f} \n'.format(y,x,t))         

Projectile(Vo,Theta,yo)