# Activity 12.1.1: Task 1
# File: ACT12p1_Task1_TEAM060_pappamr.py
# Date:    21 November 2019
# By:      Michael Pappa
# Section: 005
# Team:    060
# 
# ELECTRONIC SIGNATURE
# Michael Pappa
#
# The electronic signature above indicates the script
# submitted for evaluation is my individual work, and I
# have a general understanding of all aspects of its
# development and execution.
#
# A BRIEF DESCRIPTION OF WHAT THE SCRIPT OR FUNCTION DOES
# This program uses a taylor series loop to find the cosine of an angle in radians.

import math

d = input('Is the angle in Units of Radians or Degrees? \n')

if d == 'Radians' or 'R':
    angle = float(input('Input an angle from 0 to 2 Pi \n'))

    while angle < 0 or angle > (2 * math.pi):
        angle = float(input('Input an angle from 0 to 2 Pi \n'))

elif d == 'Degrees' or 'D':
    angle = float(input('Input an angle from 0 to 360 degrees \n'))

    while angle < 0 or angle > 360:
        angle = float(input('Input an angle from 0 to 360 \n'))
    
    if angle > 0 and angle < 360 :
        angle = math.radians(angle)

i = int(input('Input the number of terms in the Taylor Sequence to include in the series summation. \n'))

cos = 0

for k in range(i):
    cos += ((-1)**k) * (angle ** (2*k)) / (math.factorial(2 * k))

print('The Taylor Series summation for the number of terms is {0:0.6f} \n'.format(cos))