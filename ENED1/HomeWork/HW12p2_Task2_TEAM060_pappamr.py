# Activity 12.2: Task 2
# File: ACT12p2_Task2_TEAM060_pappamr.py
# Date:    21 November 2019
# By:      Michael Pappa
# Section: 005
# Team:    060
# 
# ELECTRONIC SIGNATURE
# Michael Pappa
#
# The electronic signature above indicates the script
# submitted for evaluation is my individual work, and I
# have a general understanding of all aspects of its
# development and execution.
#
# A BRIEF DESCRIPTION OF WHAT THE SCRIPT OR FUNCTION DOES
# This program uses a taylor series loop to find the cosine of an angle in radians and stops when a tolerance level is met.

import math

d = input('Is the angle in Units of Radians or Degrees? \n')

if d == 'Radians' or 'R':
    angle = float(input('Input an angle from 0 to 2 Pi \n'))

    while angle < 0 or angle > (2 * math.pi):
        angle = float(input('Input an angle from 0 to 2 Pi \n'))

elif d == 'Degrees' or 'D':
    angle = float(input('Input an angle from 0 to 360 degrees \n'))

    while angle < 0 or angle > 360:
        angle = float(input('Input an angle from 0 to 360 \n'))
    
    if angle > 0 and angle < 360 :
        angle = math.radians(angle)

t = float(input('Input a Tolerance level \n'))

cur = 0
prev = 0
k = 0

while math.fabs(prev-cur) > t or math.fabs(prev-cur) == 0:
    prev = cur
    cur += ((-1)**k) * (angle ** (2*k)) / (math.factorial(2 * k))
    k += 1
    
print('The Taylor Series summation for the number of terms is {0:0.6f} and the number of terms is {1} \n'.format(cur,k))