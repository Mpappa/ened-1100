# Homework 11.1
# File: HW11p1_Task1_TEAM060_pappamr.py
# Date:    14 November 2019
# By:      Michael Pappa
# Section: 005
# Team:    060
# 
# ELECTRONIC SIGNATURE
# Michael Pappa
#
# The electronic signature above indicates the script
# submitted for evaluation is my individual work, and I
# have a general understanding of all aspects of its
# development and execution.
#
# A BRIEF DESCRIPTION OF WHAT THE SCRIPT OR FUNCTION DOES
# Program outputs the estimated equation of best fit with m and b values

import math

X1 = float(input('Input X1 coordinate \n'))
Y1 = float(input('Input Y1 coordinate \n'))
X2 = float(input('Input X2 coordinate \n'))
Y2 = float(input('Input Y2 coordinate \n'))

axis = input('Type of axis (Rectangular, log-log, or semi-log)\n')

if axis == 'Rectangular':
    m = (Y2-Y1)/(X2-X1)
    b = Y2-(m * X2)
elif axis == 'log-log':
    m = ((math.log(Y2,10))-(math.log(Y1,10)))/((math.log(X2,10))-(math.log(X1,10)))
    b = math.exp(math.log(Y1,10)-(m * math.log(X1,10)))
else:
    m =(Y2-Y1)/(X2-X1)
    b = math.exp(math.log(Y1)-(m * math.log(X1)))

print('y = {0:.2f}x + {1:.2f} \n'.format(m,b))