# ENED 1100 Final Exam Problem 11
# Name: Michael Pappa
# 6+2:pappamr  
# M-Number:M13355492
# Section:005
# Team:060

import math
def Vcap(V,R,C,dt):
    if V < 0 or R < 0 or C < 0 or dt < 0:
        print('Invalid Inputs')
    else:
        tcharge = 5 * R * C
        print('tcharge = {0:0.2f} \n'.format(tcharge))

        k = 0
        t = 0

        while t <= tcharge:
            k * dt == t
            Vc = V(1-math.exp(-t/(R*C)))
            print('Vc = {0:0.2f} T = {1:0.2f} \n'.format(Vc,t))
            k + 1 == k
    return(Vc,t)